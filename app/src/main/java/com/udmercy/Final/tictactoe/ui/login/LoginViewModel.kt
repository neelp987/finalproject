package com.udmercy.Final.tictactoe.ui.login

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.udmercy.Final.tictactoe.R
import com.udmercy.Final.tictactoe.data.LoginRepository
import com.udmercy.Final.tictactoe.data.Result
import com.udmercy.Final.tictactoe.exceptions.AuthenticationException
import com.udmercy.Final.tictactoe.exceptions.UserNotFoundException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    fun login(username: String, password: String) {
        GlobalScope.launch {
            val result = loginRepository.login(username, password)

            if (result is Result.Success) {
                _loginResult.postValue(LoginResult(success = LoggedInUserView(displayName = result.data.userName)))
            } else {
                val errorResult = result as Result.Error
                when (errorResult.exception) {
                    is UserNotFoundException -> _loginResult.postValue(LoginResult(error = R.string.user_not_available))
                    is AuthenticationException -> _loginResult.postValue(LoginResult(error = R.string.incorrect_password))
                    else -> _loginResult.postValue(LoginResult(error = R.string.login_failed))
                }
            }
        }
    }

    fun register(username: String, password: String) {
        GlobalScope.launch {
            val result = loginRepository.register(username, password)

            if (result is Result.Success) {
                _loginResult.postValue(LoginResult(success = LoggedInUserView(displayName = result.data.userName)))
            } else {
                if ((result as Result.Error).exception is UserNotFoundException) {
                    _loginResult.postValue(LoginResult(error = R.string.user_not_available))
                } else {
                    _loginResult.postValue(LoginResult(error = R.string.login_failed))
                }
            }
        }
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}
