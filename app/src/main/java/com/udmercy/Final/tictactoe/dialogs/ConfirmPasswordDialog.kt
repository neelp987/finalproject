package com.udmercy.Final.tictactoe.dialogs

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import com.udmercy.Final.tictactoe.R
import com.udmercy.Final.tictactoe.ui.login.afterTextChanged
import kotlinx.android.synthetic.main.confirm_password_dialog_layout.view.*

class ConfirmPasswordDialog {
    companion object {
        fun show(
            context: Context,
            onSaved: (String, String) -> Unit
        ) {
            val view = LayoutInflater.from(context)
                .inflate(R.layout.confirm_password_dialog_layout, null, false)
            val etCurrentPassword = view.et_current_password
            val etNewPassword = view.et_new_password
            var currentPassword = ""
            var newPassword = ""
            etCurrentPassword.afterTextChanged { currentPassword = it }
            etNewPassword.afterTextChanged { newPassword = it }
            val builder = AlertDialog.Builder(context)
                .setTitle("Change Password")
                .setView(view)
                .setCancelable(false)
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Save") { _, _ ->
                    onSaved(currentPassword, newPassword)
                }
            val dialog = builder.create()
            dialog.show()

            val positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            positiveButton.isEnabled = false

            etNewPassword.afterTextChanged {
                val isValidPassword = it.length > 5
                positiveButton.isEnabled = isValidPassword
                view.til_new_password.error = if (isValidPassword) null
                else context.getString(R.string.invalid_password)
            }
        }
    }
}