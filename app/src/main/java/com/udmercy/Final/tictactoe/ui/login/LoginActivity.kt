package com.udmercy.Final.tictactoe.ui.login

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputEditText
import com.udmercy.Final.tictactoe.MainActivity
import com.udmercy.Final.tictactoe.R
import com.udmercy.Final.tictactoe.data.database.UserDatabase
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initViewModel()
        initLoginFields()
        loginButton.setOnClickListener { onLoginButtonClicked() }
    }

    private fun initLoginFields() {
        username.afterTextChanged { onLoginDataChange() }

        password.apply {
            afterTextChanged { onLoginDataChange() }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE -> onLoginButtonClicked()
                }
                false
            }

        }
    }

    private fun onLoginButtonClicked() {
        loading.visibility = View.VISIBLE
        loginViewModel.login(
            username.text.toString(),
            password.text.toString()
        )
    }

    private fun onLoginDataChange() {
        loginViewModel.loginDataChanged(
            username.text.toString(),
            password.text.toString()
        )
    }

    private fun onLoginResult(it: LoginResult?) {
        val loginResult = it ?: return
        loading.visibility = View.GONE

        if (loginResult.success != null) {
            updateUiWithUser(loginResult.success)
            navigateToAppMainScreen()
            return
        }

        when (loginResult.error) {
            R.string.user_not_available -> {
                askForRegistrationViaDialog()
                return
            }
            R.string.incorrect_password -> {
                showIncorrectPasswordDialog()
                return
            }
            else -> loginResult.error?.let(this::showLoginFailed)
        }
    }

    private fun navigateToAppMainScreen() {
        setResult(Activity.RESULT_OK)
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)

        //Complete and destroy login activity once successful
        finish()
    }

    private fun askForRegistrationViaDialog() {
        AlertDialog.Builder(this)
            .setMessage("User not available, do you want to sign-up with your entered credentials?")
            .setPositiveButton("Yes") { _, _ ->
                loading.visibility = View.VISIBLE
                loginViewModel.register(username.text.toString(), password.text.toString())
            }
            .setNegativeButton("Cancel", null)
            .show()
    }

    private fun showIncorrectPasswordDialog() {
        AlertDialog.Builder(this)
            .setMessage(R.string.incorrect_password)
            .setPositiveButton("ok", null)
            .show()
    }

    private fun onLoginFormStateUpdate(it: LoginFormState?) {
        val loginState = it ?: return

        // disable login button unless both username / password is valid
        loginButton.isEnabled = loginState.isDataValid

        // show error for username input
        til_username.error = if (loginState.usernameError == null) null
        else getString(loginState.usernameError)

        // show error for password input
        til_password.error = if (loginState.passwordError == null) null
        else getString(loginState.passwordError)
    }

    private fun initViewModel() {
        val userDao = UserDatabase.getDB(this).userDao()
        val loginPrefFileKey = getString(R.string.login_preferences_name)
        val loginPreferences =
            getSharedPreferences(loginPrefFileKey, Context.MODE_PRIVATE)
        val viewModelProvider =
            ViewModelProvider(this, LoginViewModelFactory(userDao, loginPreferences))
        loginViewModel = viewModelProvider.get(LoginViewModel::class.java)
        loginViewModel.loginFormState.observe(this, Observer(this::onLoginFormStateUpdate))
        loginViewModel.loginResult.observe(this@LoginActivity, Observer(this::onLoginResult))
    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName
        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to TextInputEditText components.
 */
fun TextInputEditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
