package com.udmercy.Final.tictactoe.score

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Score(val gameId: Int, val userName: String, val score: Int = 0) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}