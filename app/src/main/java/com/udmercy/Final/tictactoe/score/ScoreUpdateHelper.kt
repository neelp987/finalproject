package com.udmercy.Final.tictactoe.score

import android.content.Context
import com.udmercy.Final.tictactoe.R
import com.udmercy.Final.tictactoe.data.LoginDataSource
import com.udmercy.Final.tictactoe.data.LoginRepository
import com.udmercy.Final.tictactoe.data.database.UserDao
import com.udmercy.Final.tictactoe.data.database.UserDatabase
import com.udmercy.Final.tictactoe.score.data.ScoreDao
import com.udmercy.Final.tictactoe.score.data.ScoreDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ScoreUpdateHelper(val context: Context, val gameId: Int) {

    private lateinit var scoreDao: ScoreDao
    private lateinit var userDao: UserDao
    private lateinit var loginRepository: LoginRepository

    init {
        initDataSource()
    }

    private fun initDataSource() {
        scoreDao = ScoreDatabase.getDB(context).scoreDao()
        userDao = UserDatabase.getDB(context).userDao()
        val loginPreferences =
            context.getSharedPreferences(
                context.getString(R.string.login_preferences_name),
                Context.MODE_PRIVATE
            )
        loginRepository = LoginRepository(
            dataSource = LoginDataSource(userDao),
            loginPreferences = loginPreferences
        )
    }

    fun incrementScore() {
        GlobalScope.launch {
            val scores =
                scoreDao.getScoreByGameIdAndUser(
                    gameId,
                    loginRepository.user!!.userName
                )
            if (scores.isEmpty()) {
                val score = Score(
                    gameId,
                    loginRepository.user!!.userName,
                    1
                )
                scoreDao.insertScore(score)
            } else {
                val score = scores.first()
                scoreDao.updateScore(
                    gameId = score.gameId,
                    userName = score.userName,
                    score = score.score + 1
                )
            }
        }
    }

    fun updateHighScore(newScore: Int) {
        GlobalScope.launch {
            val scores =
                scoreDao.getScoreByGameIdAndUser(
                    gameId,
                    loginRepository.user!!.userName
                )
            if (scores.isEmpty()) {
                scoreDao.insertScore(Score(
                    gameId,
                    loginRepository.user!!.userName,
                    newScore
                ))
            } else {
                val score = scores.first()
                if (newScore > score.score) {
                    scoreDao.updateScore(
                        gameId = score.gameId,
                        userName = score.userName,
                        score = newScore
                    )
                }
            }
        }
    }
}