package com.udmercy.Final.tictactoe.ui.login

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.udmercy.Final.tictactoe.data.LoginDataSource
import com.udmercy.Final.tictactoe.data.LoginRepository
import com.udmercy.Final.tictactoe.data.database.UserDao

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class LoginViewModelFactory(
    private val userDao: UserDao,
    private val loginPreferences: SharedPreferences
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(
                loginRepository = LoginRepository(
                    dataSource = LoginDataSource(userDao),
                    loginPreferences = loginPreferences
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
