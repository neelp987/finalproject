package com.udmercy.Final.tictactoe.games

import com.udmercy.Final.tictactoe.R

class GameId {
    companion object {
        val ticTacToeGameId = 0
        val hangManGameId = 1

        fun stringIdToTitle(gameId: Int): Int? = when (gameId) {
            ticTacToeGameId -> R.string.title_activity_tic_tac_toe
            hangManGameId -> R.string.title_activity_hangman
            else -> null
        }
    }
}