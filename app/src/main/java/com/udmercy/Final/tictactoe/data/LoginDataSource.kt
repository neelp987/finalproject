package com.udmercy.Final.tictactoe.data

import com.udmercy.Final.tictactoe.data.database.UserDao
import com.udmercy.Final.tictactoe.exceptions.AuthenticationException
import com.udmercy.Final.tictactoe.exceptions.UserNotFoundException
import com.udmercy.Final.tictactoe.data.model.User
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource(private val userDao: UserDao) {

    fun login(username: String, password: String): Result<User> {
        val userByUsersName = userDao.getUserByUsersName(username)
        if (userByUsersName.isEmpty()) {
            return Result.Error(UserNotFoundException())
        } else if (userByUsersName.first().password != password) {
            return Result.Error(AuthenticationException())
        }
        try {
            return Result.Success(
                User(
                    username,
                    password
                )
            )
        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    fun register(username: String, password: String): Result<User> {
        try {
            val registeredUser =
                User(username, password)
            userDao.insertUser(registeredUser)
            return Result.Success(registeredUser)
        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    fun updatePassword(userName: String, password: String) {
        userDao.updatePassword(userName, password)
    }
}

