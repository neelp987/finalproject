package com.udmercy.Final.tictactoe.score.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.udmercy.Final.tictactoe.score.Score

@Database(entities = [Score::class], version = 1)
abstract class ScoreDatabase : RoomDatabase() {
    abstract fun scoreDao(): ScoreDao

    companion object {
        @Volatile
        private var instance: ScoreDatabase? = null
        private val LOCK = Any()

        fun getDB(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context): ScoreDatabase {
            return Room.databaseBuilder(
                context,
                ScoreDatabase::class.java, "score.db"
            ).build()
        }
    }
}