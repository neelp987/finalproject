package com.udmercy.Final.tictactoe

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.udmercy.Final.tictactoe.data.LoginDataSource
import com.udmercy.Final.tictactoe.data.LoginRepository
import com.udmercy.Final.tictactoe.data.database.UserDatabase
import com.udmercy.Final.tictactoe.dialogs.ConfirmPasswordDialog
import com.udmercy.Final.tictactoe.games.GameId
import com.udmercy.Final.tictactoe.score.Score
import com.udmercy.Final.tictactoe.score.data.ScoreDao
import com.udmercy.Final.tictactoe.score.data.ScoreDatabase
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.change_password_layout.*
import kotlinx.android.synthetic.main.score_card_layout.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * This screen allows user to change password or reset scores for the games.
 * User can navigate to this screen from MainActivity
 */
class SettingsActivity : AppCompatActivity() {
    private lateinit var loginRepository: LoginRepository
    private lateinit var scoreDao: ScoreDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        // init resources
        initDataSource()

        // init view
        initTicTacToeScoreCard()
        initHangmanScoreCard()
        et_username.text = loginRepository.user!!.userName
    }

    /**
     * Will get the reference to user data and score data.
     */
    private fun initDataSource() {
        scoreDao = ScoreDatabase.getDB(this).scoreDao()
        val userDao = UserDatabase.getDB(this).userDao()
        val loginPrefFileKey = getString(R.string.login_preferences_name)
        val loginPreferences = getSharedPreferences(
            loginPrefFileKey, Context.MODE_PRIVATE
        )
        loginRepository = LoginRepository(LoginDataSource(userDao), loginPreferences)
    }

    private fun initHangmanScoreCard() {
        GlobalScope.launch {
            val gameId = GameId.hangManGameId
            val userName = loginRepository.user!!.userName
            val score = getGameScoreFromDB(gameId, userName)
            initScoreCard(score_card_hangman, score)
        }
    }

    private fun initTicTacToeScoreCard() {
        GlobalScope.launch {
            val gameId = GameId.ticTacToeGameId
            val userName = loginRepository.user!!.userName
            val score = getGameScoreFromDB(gameId, userName)
            initScoreCard(score_card_tic_tac_toe, score)
        }
    }

    private fun getGameScoreFromDB(gameId: Int, userName: String): Score {
        val scores =
            scoreDao.getScoreByGameIdAndUser(gameId, userName)
        return if (scores.isEmpty()) Score(gameId, userName, 0)
        else scores.first()
    }

    private fun resetScore(gameId: Int, userName: String) {
        GlobalScope.launch { scoreDao.updateScore(gameId, userName, 0) }
    }

    private fun initScoreCard(view: View, score: Score) {
        runOnUiThread {
            view.apply {
                // init title
                val stringId = GameId.stringIdToTitle(score.gameId)
                val gameTitle = if (stringId == null) "" else getString(stringId)
                tv_game_title.text = gameTitle

                // init score
                val scoreText = "Score - ${score.score}"
                tv_game_score.text = scoreText

                // init reset button
                reset_button.setOnClickListener {
                    askConfirmationToResetScore(view, gameTitle) {
                        resetScore(score.gameId, score.userName)
                    }
                }
            }
        }
    }

    private fun askConfirmationToResetScore(
        view: View,
        gameTitle: String,
        onResetClicked: () -> Unit
    ) {
        AlertDialog.Builder(view.context)
            .setMessage("Are you sure you want to reset score for ${gameTitle}?")
            .setPositiveButton("Yes, reset") { _, _ -> onResetClicked() }
            .setNegativeButton("No", null)
            .show()
    }

    fun onChangePasswordClicked(view: View) {
        ConfirmPasswordDialog.show(this) { currentPassword: String, newPassword: String ->
            onPasswordResetRequested(currentPassword, newPassword)
        }
    }

    private fun onPasswordResetRequested(currentPassword: String, newPassword: String) {
        val passwordInDB = loginRepository.user!!.password
        val isValidPasswordReceived = passwordInDB == currentPassword
        if (isValidPasswordReceived) {
            updatePasswordInDB(newPassword)
            showSuccessToastForPasswordReset()
        } else {
            showPasswordResetFailDialog()
        }
    }

    private fun showSuccessToastForPasswordReset() {
        Toast.makeText(
            this,
            "Password changed successfully",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun updatePasswordInDB(newPassword: String) {
        GlobalScope.launch {
            loginRepository.updatePassword(newPassword)
        }
    }

    private fun showPasswordResetFailDialog() {
        AlertDialog.Builder(this)
            .setTitle("Password reset fail")
            .setMessage("Entered current password is incorrect!!")
            .setNegativeButton("Ok", null)
            .show()
    }
}
