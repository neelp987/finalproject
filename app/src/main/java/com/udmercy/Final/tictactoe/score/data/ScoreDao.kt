package com.udmercy.Final.tictactoe.score.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.udmercy.Final.tictactoe.score.Score

@Dao
interface ScoreDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertScore(user: Score)

    @Query("UPDATE Score SET score=:score WHERE gameId=:gameId AND userName=:userName")
    fun updateScore(gameId: Int, userName: String, score: Int)

    @Query("SELECT * FROM Score WHERE gameId=:gameId AND userName=:userName")
    fun getScoreByGameIdAndUser(gameId: Int, userName: String):List<Score>
}