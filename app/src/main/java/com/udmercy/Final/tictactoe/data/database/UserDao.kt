package com.udmercy.Final.tictactoe.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.udmercy.Final.tictactoe.data.model.User

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User)

    @Query("SELECT * FROM User WHERE userName=:userName")
    fun getUserByUsersName(userName: String): List<User>

    @Query("UPDATE User SET password=:password WHERE userName=:userName")
    fun updatePassword(userName: String, password: String)
}