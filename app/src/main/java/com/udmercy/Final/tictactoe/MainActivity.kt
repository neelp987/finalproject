package com.udmercy.Final.tictactoe

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.udmercy.Final.tictactoe.games.hangman.HangmanActivity
import com.udmercy.Final.tictactoe.games.tictactoe.TicTacToeActivity

/**
 * This activity is the dashboard screen.
 *
 * Users can navigate to TicTacToeActivity, HangmanActivity or
 * SettingsActivity from this screen.
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onTicTacToeClicked(view: View) = openActivity(TicTacToeActivity::class.java)

    fun onHangManClicked(view: View) = openActivity(HangmanActivity::class.java)

    fun onSettingsClicked(view: View) = openActivity(SettingsActivity::class.java)

    /**
     * Opens activity provided as an dependency.
     */
    private fun <T>openActivity(activityToOpen: Class<T>) {
        val intent = Intent(this, activityToOpen)
        startActivity(intent)
    }
}