package com.udmercy.Final.tictactoe.data

import android.content.SharedPreferences
import com.udmercy.Final.tictactoe.data.model.User

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class LoginRepository(
    private val dataSource: LoginDataSource,
    private val loginPreferences: SharedPreferences
) {

    // in-memory cache of the loggedInUser object
    var user: User? = null
        private set

    companion object {
        private val PREF_KEY_CURRENT_USER = "current_user"
        private val PREF_KEY_PASSWORD = "current_user_password"
    }

    init {
        val cachedUserId = loginPreferences.getString(PREF_KEY_CURRENT_USER, null)
        val cachedPassword = loginPreferences.getString(PREF_KEY_PASSWORD, null)
        user =
            if (cachedUserId == null || cachedPassword == null) null else User(
                cachedUserId,
                cachedPassword
            )
    }

    fun login(username: String, password: String): Result<User> {
        // handle login
        val result = dataSource.login(username, password)

        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }

        return result
    }

    fun register(username: String, password: String): Result<User> {
        // handle login
        val result = dataSource.register(username, password)

        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }

        return result
    }

    private fun setLoggedInUser(loggedInUser: User) {
        this.user = loggedInUser
        loginPreferences.edit().putString(PREF_KEY_CURRENT_USER, loggedInUser.userName).apply()
        loginPreferences.edit().putString(PREF_KEY_PASSWORD, loggedInUser.password).apply()
    }

    fun updatePassword(newPassword: String) {
        user!!.password = newPassword
        dataSource.updatePassword(user!!.userName, user!!.password)
        loginPreferences.edit().putString(PREF_KEY_PASSWORD, newPassword).apply()
    }
}
