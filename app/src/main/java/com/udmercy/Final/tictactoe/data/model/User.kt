package com.udmercy.Final.tictactoe.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(@PrimaryKey val userName: String, var password: String)